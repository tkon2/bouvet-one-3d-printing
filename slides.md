# Before the print

# Open source things
* Open formats
* Free software
* Thingiverse

# Basic steps
* Modelling
* Slicing
* Printing

# File formats
* STL
* GCODE

# Modelling
![scad](./scad.png)
* OpenSCAD
* Blender
* Most can export STL

# STL
![stl](./stl.png)
* ASCII or binary
* Defines triangles
* Clockwise rule
* Duplicate vertices

# Slicing
![before](./before_slicing.png)
* Printers are dumb
* Adjustments
![after](./after_slicing.png)
* Send to printer

# GCODE
![gcode](./gcode.png)
* ASCII
* No loops or functions
* Originally for CNC
* Exact movements

# Printing
* SD cards, USB
* Octoprint
![house](./house.jpg)
![hive](./hive.jpg)
![gear](./gear.jpg)
![gear](./skull.jpg)
* Expect problems
![blob](./blob.jpg)

# Rain cover clip
* Problem
* Solution
![clip](./bag_clip.jpg)
* You can have one!

# Would you like to know more?
* Two printers available
* (mostly)
* 500g free every month
* Courses ran regularly

