CubeHeight = 8;
CubeSize = 10;
RoofHeight = 5;

CubePoints = [
  [ 0,        0,        0          ],  //0
  [ CubeSize, 0,        0          ],  //1
  [ CubeSize, CubeSize, 0          ],  //2
  [ 0,        CubeSize, 0          ],  //3
  [ 0,        0,        CubeHeight ],  //4
  [ CubeSize, 0,        CubeHeight ],  //5
  [ CubeSize, CubeSize, CubeHeight ],  //6
  [ 0,        CubeSize, CubeHeight ],   //7
  
  // Roof
  [ 0,        CubeSize/2, CubeHeight + RoofHeight ], // 8
  [ CubeSize, CubeSize/2, CubeHeight + RoofHeight ]  // 9
];

CubeFaces = [
  [0,1,2,3],  // bottom
  [4,5,1,0],  // front
  [5,9,6,2,1],  // right
  [6,7,3,2],  // back
  [7,8,4,0,3],  // left
  [8,9,5,4],
  [9,8,7,6],
  ]; 
  
polyhedron( CubePoints, CubeFaces );
